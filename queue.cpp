#include "queue.h"
#include <iostream>


void initQueue(queue* q, unsigned int size)
{
	q->element = new int[size];
	q->maxSize = size;
	q->count = 0;
}

void cleanQueue(queue* q)
{
	delete[] q->element;
	delete(q);
}

void enqueue(queue* q, unsigned int newValue)
{
	int i = 0;
	if (!isFull(q))
	{
		for (i = q->count; i >= 0; i--)
		{
			if (i == 0)
			{
				q->element[i] = newValue;
			}
			else
			{
				q->element[i] = q->element[i - 1];
			}
		}
		q->count++;
	}
	else
	{
		printf("The queue is full");
	}

}


int dequeue(queue* q) // return element in top of queue, or -1 if empty
{
	int topElement = 0;
	if (!isEmpty)
	{
		topElement = q->element[0];
		q->element[0] = 0;
		int i = 0;
		for (i = 1; i < q->count; i++)
		{
			q->element[i - 1] = q->element[i];
			q->element[i] = 0;
		}
		q->count--;
	}
	else
	{
		topElement = -1;
	}
	return topElement;
}

int isEmpty(queue* q)
{
	return q->count == 0;
}

bool isFull(queue* q)
{
	return q->maxSize == q->count;
}
