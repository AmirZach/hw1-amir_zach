#include "linkedList.h"
#include <iostream>

/*
Function will add node to the linked list
input: pointer to the head of the list, new num
output: none
*/
void addNum(numberNode** head, int num)
{
	numberNode* newNumber = new numberNode;
	newNumber->num = num;
	newNumber->next = NULL;

	if (!(*head))
	{
		*head = newNumber;
	}
	else
	{
		newNumber->next = *head; //making the first node as the second
		*head = newNumber; //putting the new node as the first
	}
}

/*
Function will remove node from the linked list
input: pointer to the head of the list
output: none
*/
void removeNum(numberNode** head)
{
	numberNode* temp = new numberNode;
	temp = (*head)->next; //saving the second node
	delete(*head); //removing the first
	*head = temp; //putting the second as the first
}