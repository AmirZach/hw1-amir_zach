#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	int* element;
	int maxSize;
	int count;
} queue;

void initQueue(queue* q, unsigned int size);

void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);

int dequeue(queue* q); // return element in top of queue, or -1 if empty
int isEmpty(queue* q);

bool isFull(queue* q);

#endif /* QUEUE_H */
