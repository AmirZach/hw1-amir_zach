#pragma once

typedef struct numberNode
{
	int num;
	struct numberNode* next;
}numberNode;

void addNum(numberNode** head, int num);
void removeNum(numberNode** head);


