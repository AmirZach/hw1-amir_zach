#include "utils.h"
#define TEN 10
#include <iostream>
using namespace std;

/*
Function will get array and reverse the order of it
input: the array, the size of the array
output: none
*/
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	int i = 0;
	initStack(s); 

	for (i = 0; i < size; i++) //getting all the array into the stack
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; i++) //popping all the numbers from the stack to the array
	{
		nums[i] = pop(s);
	}

}

/*
Function will get 10 numbers to an array and reverse the order
input: none
output: the array
*/
int* reverse10()
{
	int* a = new int[TEN];
	int i = 0;

	cout << "Please enter 10 numbers: ";
	for (i = 0; i < TEN; i++)
	{
		cin >> a[i];
		getchar();
	}

	reverse(a, TEN);

	return a;
}