#include "stack.h"
#include <iostream>

/*
Function will intialize stack
input: stack s
output: none
*/
void initStack(stack* s)
{
	s->head = new numberNode;
	s->head->num = 0;
	s->head->next = NULL;
	s->currIndex = 0;
}

/*
Function will insert new node
input: stack s, new elemnt
output: none
*/
void push(stack* s, unsigned int element)
{
	numberNode* newNumber = new numberNode;
	newNumber->num = element;
	newNumber->next = NULL;
	numberNode* temp = NULL;

	if (!(s->currIndex))
	{
		s->head = newNumber;
	}
	else
	{
		temp = s->head;
		s->head = newNumber;
		newNumber->next = temp;
	}
	s->currIndex++;
}

/*
Function will take out the first node
input: stack s
output: the value that took out
*/
int pop(stack* s)
{
	int toReturn = -1;
	numberNode* temp = NULL;

	if ((s->head))
	{
		temp = s->head->next; //saving the second node
		toReturn = s->head->num;
		delete(s->head); //removing the first
		s->head = temp; //putting the second as the first
	}

	return toReturn;
}

/*
Function will clean the stack
input: stack s
output: none
*/
void cleanStack(stack* s)
{
	numberNode* temp = NULL;
	numberNode* curr = s->head;
	while (curr)
	{
		temp = curr;
		curr = curr->next;
		delete(temp);
	}

	s->head = NULL;
	delete(s);
}